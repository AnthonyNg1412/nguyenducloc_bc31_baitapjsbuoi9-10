var staff = function(_tknv, _name, _email, _password, _datepicker, _luongCB, _chucvu, _gioLam) {

    this.tknv = _tknv; 
    this.name = _name;
    this.email = _email; 
    this.password = _password; 
    this.datepicker = _datepicker; 
    this.luongCB = _luongCB; 
    this.chucvu = _chucvu; 
    this.gioLam = _gioLam;
    this.payCheck = function () {
        if (this.chucvu == "Sếp") {
            return (this.luongCB * 3); 
        } else if (this.chucvu == "Trưởng phòng") {
            return (this.luongCB * 2); 
        } else if (this.chucvu == "Nhân viên") {
            return (this.luongCB); 
    }}
    this.rank = function () {
        if (this.gioLam < 160) {
            return "Nhân viên trung bình"
        } else if (this.gioLam >= 160) {
            return "Nhân viên khá"
        } else if (this.gioLam >= 176) {
            return "Nhân viên giỏi"
        } else if (this.gioLam >= 192) {
            return "Nhân viên xuất sắc"
        }};
};