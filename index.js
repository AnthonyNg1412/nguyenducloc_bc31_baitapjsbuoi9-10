var staffList = []; 

var dataJson = localStorage.getItem("STAFFLIST");
if (dataJson !== null) {

    var arrayStaffList = JSON.parse(dataJson);

    for (i = 0; i < arrayStaffList.length; i++) {
        var item = arrayStaffList[i]
        var stf = new staff (
            item.tknv, 
            item.name, 
            item.email, 
            item.password, 
            item.datepicker, 
            item.luongCB, 
            item.chucvu, 
            item.gioLam
            ); 
        staffList.push(stf);
    }

    renderStaffList(staffList)
}

var luuLocalStorage = function(staffList) {
    var staffListJSON = JSON.stringify(staffList);
   localStorage.setItem("STAFFLIST", staffListJSON);
} 

function renderStaffList(arrayStaffList) {
    
    var contentHTML = ""; 
    for (i = 0; i < arrayStaffList.length; i++) {

        var stf = arrayStaffList[i]
        var contentTR = `<tr>
                            <td>${stf.tknv}</td>
                            <td>${stf.name}</td>
                            <td>${stf.email}</td>
                            <td>${stf.datepicker}</td>
                            <td>${stf.chucvu}</td>
                            <td>${stf.payCheck()}</td>
                            <td>${stf.rank()}</td>
                            <td>
                            <button class ="btn btn-danger" onclick="deleteStaff(${stf.tknv})">Xóa</button>
                            <button class ="btn btn-primary" onclick="getStaffInfo(${stf.tknv}) " data-toggle="modal"
                            data-target="#myModal"
                            >Sửa</button>
                            </td>
                            
                        </tr>`
        contentHTML += contentTR;
    }

    document.getElementById("tableDanhSach").innerHTML = contentHTML
}

function addStaff() {
   var stf = staffInput();

    var valid = true; 

    valid &= kiemTraSo(stf.tknv, "#error_all_number_tknv", "Tài Khoản nhân viên") & kiemTraDoDai (stf.tknv, "#error_min_max_value_tknv", "Tài Khoản nhân viên", 4, 6); 

    valid &= kiemTraTatCaKyTu(stf.name, "#error_required_all_letter_name", "Tên nhân viên");

    valid &= kiemTraEmail(stf.email, "#error_required_email","Email nhân viên"); 

    valid &= kiemTraPassWord(stf.password, "#error_required_password", "Mật khẩu");

    valid &= kiemTraRong(stf.datepicker, "#error_required_datepicker", "Ngày vào làm");

    valid &= kiemTraGiaTri(stf.luongCB,"#error_min_max_value_luongCB", "Lương cơ bản", 1000000, 20000000);

    valid &= kiemTraGiaTri(stf.gioLam, "#error_min_max_value_gioLam", "Giờ làm", 80, 200);

    if (!valid) {
        return;
    }

   staffList.push(stf); 
   renderStaffList(staffList);
   
   document.getElementById("myForm").reset();

   luuLocalStorage(staffList);
}

function deleteStaff(tknv) {
    
    var viTri = findLocation(staffList, tknv)
    
    staffList.splice(viTri,1); 
    renderStaffList(staffList);

    luuLocalStorage(staffList)
}

function getStaffInfo(tknv) {

    var viTri = findLocation(staffList, tknv);

    if(viTri == -1) {
        return; 
    }

    var staff = staffList[viTri]
    document.getElementById("tknv").disabled = true;
    
    rederStaffInfo(staff)
}

function updateStaffInfo() {
    document.getElementById("tknv").disabled = false;

    var stfUpdate = staffInput();

    var viTri = findLocation(staffList, tknv.value);

    if(viTri == -1) {
        return; 
    }

    var valid = true; 

    valid &= kiemTraSo(stfUpdate.tknv, "#error_all_number_tknv", "Tài Khoản nhân viên") & kiemTraDoDai (stfUpdate.tknv, "#error_min_max_value_tknv", "Tài Khoản nhân viên", 4, 6); 

    valid &= kiemTraTatCaKyTu(stfUpdate.name, "#error_required_all_letter_name", "Tên nhân viên");

    valid &= kiemTraEmail(stfUpdate.email, "#error_required_email","Email nhân viên"); 

    valid &= kiemTraPassWord(stfUpdate.password, "#error_required_password", "Mật khẩu");

    valid &= kiemTraRong(stfUpdate.datepicker, "#error_required_datepicker", "Ngày vào làm");

    valid &= kiemTraGiaTri(stfUpdate.luongCB,"#error_min_max_value_luongCB", "Lương cơ bản", 1000000, 20000000);

    valid &= kiemTraGiaTri(stfUpdate.gioLam, "#error_min_max_value_gioLam", "Giờ làm", 80, 200);

    if (!valid) {
        return;
    }

    staffList[viTri] = stfUpdate

    renderStaffList(staffList);
    
    luuLocalStorage(staffList);
 
    document.getElementById("myForm").reset();
}

function searchStaff() {
    
}