function findLocation(staffList, tknv) {
    return staffList.findIndex(function(item) {
        return item.tknv == tknv;
    });
}

var staffInput = function() {
    var tknv = document.getElementById("tknv").value; 
    var name = document.getElementById("name").value; 
    var email = document.getElementById("email").value; 
    var password = document.getElementById("password").value; 
    var datepicker = document.getElementById("datepicker").value; 
    var luongCB = document.getElementById("luongCB").value * 1; 
    var chucvu = document.getElementById("chucvu").value; 
    var gioLam = document.getElementById("gioLam").value * 1; 

    var stf = new staff (tknv, name, email, password, datepicker, luongCB, chucvu, gioLam)

    return stf;
}

var rederStaffInfo = function(stf) {
    document.getElementById("tknv").value = stf.tknv;
    document.getElementById("name").value = stf.name;
    document.getElementById("email").value = stf.email;
    document.getElementById("password").value = stf.password;
    document.getElementById("datepicker").value = stf.datepicker;
    document.getElementById("luongCB").value = stf.luongCB;
    document.getElementById("chucvu").value = stf.chucvu;
    gioLam = document.getElementById("gioLam").value = stf.gioLam;
}; 
